import express from 'express'
import jwt_decode from 'jwt-decode'

// stage
// const server = 'https://uaf-registry-service.stg.in.affinidi.io'

// prod
const server = 'https://uaf-registry-service.in.affinidi.io'

const app = express()

// configure express to parse JSON automatically
app.use(express.json())

// configure express to use 4 spaces when sending JSON as response
app.set('json spaces', 4)

const port = 3000

app.post('/jwtDecode', (req, res) => {
  console.log('input', req.body)
  const output = jwt_decode(req.body.jwt)
  console.log('output', output)
  res.json(output)
});

app.post('/jsonFormat', (req, res) => {
  console.log('input', req.body)
  console.log('json', req.body.json)
  res.send(req.body.json)
});

async function getBaseConfigId(adminAPIKey: string, tenantID: string): Promise<string> {

  console.log('in getBaseConfigId, key:', adminAPIKey, 'tenantID', tenantID)

  const response = await fetch(server + '/api/v1/usecase/list/' + tenantID, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', 'admin-api-key': adminAPIKey },
  })
  console.log('got usecase list')
  if (response.ok) {
    console.log('got usecase list, successfully')
    const responseObj = await response.json()
    // console.log('response obj', JSON.stringify(responseObj, null, 4))
    // some tenants do not have any centers
    if (responseObj['data'].length === 0) {
      return ''
    }
    // return first usecase's sourcePlatformId
    return responseObj['data'][0]['sourcePlatformId']
  }
  return ''
}

app.post('/centreList', async (req, res) => {
  console.log('input', req.body)
  const response = await fetch(server + '/api/v1/usecase/list/' + req.body.tenantId, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', 'admin-api-key': req.body.apiKey },
  })
  if (response.ok) {
    const responseObj = await response.json()
    console.log('response obj', JSON.stringify(responseObj, null, 4))
    const centreInfo = responseObj['data'].map((t) => {
      return { 'id': t['_id'], 'name': t['useCaseName'], 'metadata': t['metadata'] }
    })
    console.log('output', centreInfo)
    return res.json(centreInfo)

  }
  res.sendStatus(500)
});

app.post('/tenantList', async (APIrequest, APIresponse) => {
  console.log('API request', APIrequest.body)
  // await new Promise(resolve => setTimeout(resolve, 5000));
  const tenantList = await fetch(server + '/api/v1/tenant/list', {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', 'admin-api-key': APIrequest.body.apiKey },
  })
  if (tenantList.ok) {
    const responseObj = await tenantList.json()
    const tenantPromiseList = responseObj['data'].map(async (t) => {
      const baseConfigId = await getBaseConfigId(APIrequest.body.apiKey, t['_id'])
      return { 'id': t['_id'], 'name': t['name'], 'baseConfigId': baseConfigId }
    })
    const consolidatedOutput = await Promise.all(tenantPromiseList)
    console.log('output', consolidatedOutput)
    return APIresponse.json(consolidatedOutput)
  }
  APIresponse.sendStatus(500)
});

app.post('/centreCreate', async (req, res) => {
  console.log('input', req.body)
  const request = {
    'baseConfigId': req.body.baseConfigId,
    'learnetSeedData': [
      {
        'centerName': req.body.name,
        'centerState': req.body.state,
        'centerTcId': req.body.TcId,
        'centerTpId': req.body.TpId,
        'centerDistrictName': req.body.district,
        'centerInstituteHead': req.body.centreHead,
        'centerMobileNo': req.body.centreMobile,
        'centerEmail': req.body.centreEmail,
        'centerLocation': req.body.centreLocation,
        'centerLocationURL': req.body.centreLocationUrl,
        'course': req.body.courses.split('\n')
      }
    ]
  }
  console.log('request', request)
  const response = await fetch(server + '/api/v1/tenant/learnet-center-onboarding', {
    method: 'POST',
    headers: { 'accept': 'application/json', 'Content-Type': 'application/json', 'admin-api-key': req.body.apiKey },
    body: JSON.stringify(request)
  })
  if (response.ok) {
    const responseObj = await response.json()
    console.log('response', responseObj)
    if (responseObj.centerDetails.length == 0) {
      return res.status(500).send({
        message: 'The specified centre already exists.'
      })
    }
    return res.json(responseObj.centerDetails[0])
  }
  return res.status(500).send({
    message: 'Failed to create centre.'
  })
});

app.post('/centreMetadataUpdate', async (req, res) => {
  console.log('input', req.body)
  const request = {
    'centerName': req.body.name,
    'centerState': req.body.state,
    'centerTcId': req.body.TcId,
    'centerTpId': req.body.TpId,
    'centerDistrictName': req.body.district,
    'centerInstituteHead': req.body.centreHead,
    'centerMobileNo': req.body.centreMobile,
    'centerEmail': req.body.centreEmail,
    'centerLocation': req.body.centreLocation,
    'centerLocationURL': req.body.centreLocationUrl,
  }
  console.log('request', request)
  const response = await fetch(server + '/api/v1/usecase/update-metdata/' + req.body.usecaseId, {
    method: 'POST',
    headers: { 'accept': 'application/json', 'Content-Type': 'application/json', 'admin-api-key': req.body.apiKey },
    body: JSON.stringify(request)
  })
  if (response.ok) {
    const responseObj = await response.json()
    console.log('response', responseObj)
    return res.json(responseObj.data.metadata)
  } else {
    console.log('response', response)
  }
  res.sendStatus(500)
});

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});

