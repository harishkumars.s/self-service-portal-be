"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const jwt_decode_1 = __importDefault(require("jwt-decode"));
const app = (0, express_1.default)();
// configure express to parse JSON automatically
app.use(express_1.default.json());
// configure express to use 4 spaces when sending JSON as response
app.set('json spaces', 4);
const port = 3000;
app.post('/jwtDecode', (req, res) => {
    console.log('in jwt decode');
    console.log('input', req.body.jwt);
    const output = (0, jwt_decode_1.default)(req.body.jwt);
    console.log('output', output);
    res.json(output);
});
app.post('/centerList', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('in center list');
    console.log('input', req.body);
    const response = yield fetch('https://uaf-registry-service.stg.in.affinidi.io/api/v1/usecase/list/' + req.body.tenantId, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'admin-api-key': req.body.apiKey },
    });
    if (response.ok) {
        const responseObj = yield response.json();
        const centerInfo = responseObj['data'].map((t) => {
            return { 'id': t['_id'], 'name': t['useCaseName'], };
        });
        console.log(centerInfo);
        return res.json(centerInfo);
    }
    res.sendStatus(500);
}));
app.post('/tenantList', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('in tenant list');
    console.log('input', req.body);
    const response = yield fetch('https://uaf-registry-service.stg.in.affinidi.io/api/v1/tenant/list', {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'admin-api-key': req.body.apiKey },
    });
    if (response.ok) {
        const responseObj = yield response.json();
        const tenantInfo = responseObj['data'].map((t) => {
            return { 'id': t['_id'], 'name': t['name'], };
        });
        return res.json(tenantInfo);
    }
    res.sendStatus(500);
}));
app.post('/tenantCreate', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('in tenant create');
    console.log('input', req.body);
    const request = {
        'baseConfigId': req.body.baseConfigId,
        'learnetSeedData': [
            {
                'centerName': req.body.name,
                'centerState': req.body.state,
                'centerTcId': req.body.TcId,
                'centerTpId': req.body.TpId,
                'centerDistrictName': req.body.district,
                'centerInstituteHead': req.body.centerHead,
                'centerMobileNo': req.body.centerMobile,
                'centerEmail': req.body.centerEmail,
                'centerLocation': req.body.centerLocation,
                'centerLocationUrl': req.body.centerLocationUrl,
                'course': req.body.courses.split('\n')
            }
        ]
    };
    console.log('request', request);
    const response = yield fetch('https://uaf-registry-service.stg.in.affinidi.io/api/v1/tenant/learnet-center-onboarding', {
        method: 'POST',
        headers: { 'accept': 'application/json', 'Content-Type': 'application/json', 'admin-api-key': req.body.apiKey },
        body: JSON.stringify(request)
    });
    if (response.ok) {
        const responseObj = yield response.json();
        return res.json(responseObj.centerDetails[0]);
    }
    res.sendStatus(500);
}));
app.listen(port, () => {
    return console.log(`Express is listening at http://localhost:${port}`);
});
//# sourceMappingURL=app.js.map